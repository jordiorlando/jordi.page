---
title: "Zeroform Design"
weight: 1
resources:
    - src: zeroform-light.png
      title: Zeroform Design - light
      params:
          weight: 1
    - src: zeroform-dark.png
      title: Zeroform Design - dark
      params:
          weight: 2
    - src: sam-ism-front.jpg
      title: SAM-ISM - front
      params:
          weight: 3
    - src: sam-ism-back.jpg
      title: SAM-ISM - back
      params:
          weight: 4
    - src: sam-ism-finger.jpg
      title: SAM-ISM - finger
      params:
          weight: 5
project_timeframe: 2021
---

[Zeroform Design](https://zeroform.design) founded 01/01/2021.

Our first product is SAM-ISM, a tiny SAMD21 dev board with an integrated packet radio module.

{{< tweet 1394682920057458688 >}}
